#!/usr/bin/env python
# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.core.exceptions import PermissionDenied
from django.core.mail import send_mail
from django.core import validators
from django.db import models
from django.db.models.manager import EmptyManager
from django.utils.crypto import get_random_string, salted_hmac
from django.utils import six
from django.utils.translation import ugettext_lazy as _
from django.utils import timezone
from django.contrib import auth
from django.contrib.auth.hashers import (
	check_password, make_password, is_password_usable)
from django.contrib.auth.signals import user_logged_in
from django.contrib.contenttypes.models import ContentType
from django.utils.encoding import python_2_unicode_compatible
from django.db import models
from django.contrib.auth.models import Group, User
from django.contrib.auth.models import *
from django.core.validators import ValidationError


class FuncionarioManager(BaseUserManager):
	#Função para criar Funcionario
	def create_user(self, username, password, **extra_fields):
		now = timezone.now()
		if not username:
			raise ValueError('O usuário deve ter um Login')
		user = self.model(username=username, is_staff = is_staff, is_active=True,
			is_superuser = False, data_cadastro=now, **extra_fields)	#Seta user com um aOBJETO criado com parametros passados.
		user.set_password(password)	#Salva senha usando um modelo de criptografia.
		user.save(using=self._db)	#Salva o OBJETO no banco de dados.
		return user

	#Função para criar superFuncionario(admin)
	def create_superuser(self, username, password, **extra_fields):
		if not username:
			raise ValueError('O administrador deve ter um Login')
		now = timezone.now()
		user = self.model(username=username, is_staff=True, is_active=True,
			is_superuser=True, data_cadastro=now, **extra_fields)

		user.set_password(password)	#Salva senha usando um modelo de criptografia.
		user.is_admin = True		#Seta o OBJETO como administrador.
		user.save(using=self._db)	#Salva o OBJETO no banco de dados.

		return user
#-----------------------------------------------------------------------------------------#
class Pessoa(AbstractBaseUser):
	username	= models.CharField(_('username'), max_length=30, unique=True,
		help_text	= _('Required. 30 characters or fewer. Letters, digits and @/./+/-/_ only.'),	#Texto de ajuda.
		validators	= [ validators.RegexValidator(r'^[\w.@+-]+$',_('Enter a valid username. '	#Texto de erro,
		                                                             'This value may contain only letters, numbers and @/./+/-/_ characters.'), 'invalid'),],	#caso preenchido incorretamente
		error_messages	= {'unique': _("A user with that username already exists."),})			#Texto de erro, caso exista usuarios iguais

	nome		= models.CharField(_('Nome'), max_length=30, blank=True)
	sobrenome	= models.CharField(_('Sobrenome'), max_length=30, blank=True)
	is_staff = models.BooleanField(default = False,	 blank=True)
	is_active = models.BooleanField(_('Ativo'), default=True,
		help_text=_('Designates whether this user should be treated as '
		            'active. Unselect this instead of deleting accounts.'), blank=True)

	def get_full_name(self):	#Retorna nome completo.
		full_name = '%s %s' % (self.nome, self.sobrenome)
		return full_name.strip()

	def get_short_name(self):	#Retorna o primeiro nome.
		return self.nome

#-----------------------------------------------------------------------------------------#
class FuncionarioAbstract(PermissionsMixin, Pessoa):
	data_cadastro	= models.DateTimeField(_('Data de cadastro'), default=timezone.now)
	objects		= FuncionarioManager()

	USERNAME_FIELD	= 'username'	#Define qual atributo sera o usarname do django.
	REQUIRED_FIELDS = ['nome', 'sobrenome']	#Atributos requiridos para cadastro.

	class Meta:
		verbose_name		= _('Funcionario')	#Define nome no singular.
		verbose_name_plural		= _('Funcionarios')	#Define nome no plural.
		abstract		= True		#Define como classe abstrata.
#-----------------------------------------------------------------------------------------#
class Funcionario(FuncionarioAbstract):
	primeiroLogin = models.BooleanField(default=True)
	trocarSenha = models.BooleanField(default=False)
	ultimaModificaoSenha = models.DateTimeField(_('Data da ultima modificacao da senha'), default=timezone.now())

	class Meta(FuncionarioAbstract.Meta):
		swappable = 'AUTH_USER_MODEL'

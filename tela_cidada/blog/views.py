from django.shortcuts import render
from django.db import connection
#from blog.models import Post
pag=1;
def DictFecthAll(cursor):
	desc = cursor.description
	return [
		dict(zip([col[0] for col in desc], row))
		for row in cursor.fetchall()
	]
# Create your views here.
def pagination (request):
	
	#Conectar com o DB
	con = connection.cursor()
	
	#Obter quantidade de postagens
	con.execute("SELECT COUNT(*) AS quant FROM blog_postagem")
	quantidade = DictFecthAll(con)
	quant = quantidade[0]['quant']
	
	#obter a linha do post da pagina
	con.execute("select * from TELA_CIDADA.blog_postagem LIMIT %s,1",pag)
	
	#Desconectar do DB	
	con.close()
	
	return render(request, 'xhtml/pagination.html', {"quantidade":quant})

def get_page(request):
	if request and request.is_ajax():
		current_page = request.POST['page']
		pag=current_page
		return render(request, 'xhtml/pagination.html', {})

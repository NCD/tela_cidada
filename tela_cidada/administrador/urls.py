#!/usr/bin/env python
# -*- coding: utf-8 -*-
from django.conf.urls import patterns, include, url
from django.conf import settings
from django.conf.urls.static import static
from django.contrib import admin

urlpatterns = patterns('administrador.views',
	url(r'^$', 'loginAdmin', name = 'login'),
	url(r'^login/', 'loginAdmin', name = 'login'),
	url(r'^home/', 'home', name = 'home'),
	url(r'^criarPost/', 'criarPost', name = 'criarPost'),
	url(r'^editarPost/([^/]+)', 'editarPost', name = 'editarPost'),
	url(r'^vizualizarPosts/', 'vizualizarPosts', name = 'vizualizarPosts'),

) + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

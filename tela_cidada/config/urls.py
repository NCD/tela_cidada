from django.conf.urls import patterns, include, url
from django.conf import settings
from django.conf.urls.static import static
from django.contrib import admin

urlpatterns = patterns('',
	#url(r'^admin/', include(admin.site.urls)),

	#base
	url(r'', include('base.urls')),
	#blog
	url(r'^blog/', include('blog.urls')),
	#administrador
	url(r'^administrador/', include('administrador.urls')),
	#Midias
	(r'^midia/(?P<path>.*)$', 'django.views.static.serve',
		{
			'document_root':settings.MEDIA_ROOT
		}
	),
) + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

if settings.DEBUG:
    urlpatterns += patterns('',
        url(r'^media/(?P<path>.*)$',
            'django.views.static.serve',
            {'document_root': settings.MEDIA_ROOT, }),
    )

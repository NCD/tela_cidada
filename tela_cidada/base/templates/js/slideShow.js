var slideIndex = 1;
var nImagens = 3;

function showSlides(n) 
{
	var i;
	var fundoSlideShow = $('#FundoSlideShow');
	fundoSlideShow.css({'background':'url(/midia/img' + n + '.jpg) no-repeat'});
	fundoSlideShow.css({'background-size': '100% 100%'});
}

function plusSlides(n) 
{
	slideIndex += n;
	
	if(slideIndex > 3)
		slideIndex = 1;
		
	if(slideIndex < 1)
		slideIndex = 3;
		
	showSlides(slideIndex);
	console.log(slideIndex);
}

$(document).ready(function()
{		
	showSlides(1);
});

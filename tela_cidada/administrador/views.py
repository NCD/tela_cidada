from django.shortcuts import render
from django.contrib.auth import login
from django.contrib.auth import logout
from django.contrib.auth import authenticate
from django.shortcuts import render_to_response, render, redirect
from django.http import HttpResponseRedirect, HttpResponse, Http404
import json
import datetime
from django.db import connection
from django.utils import timezone

def DictFetchAll(cursor):
	desc = cursor.description
	return [
		dict(zip([col[0] for col in desc], row))
		for row in cursor.fetchall()
	]

def loginAdmin(request):
	response = HttpResponse()
	if request and request.is_ajax():
		funcionario = authenticate(username = request.POST['login'], password = request.POST['senha'])
		if not funcionario: # verifica se o usuario esta logado, se estiver ele sera deslogado da conta.
			response.write(json.dumps('/administrador'))
		else:
			if funcionario.is_active:
				login(request, funcionario)
            	response.write(json.dumps('/administrador/home'))
            	
		return response
	return render(request, 'xhtml/login.xhtml', {})

def home(request):
	return render(request, 'xhtml/homeAdmin.xhtml', {})

def criarPost(request):
	print request
	if not request.user.is_authenticated(): # verifica se o usuario esta logado, se estiver ele sera deslogado da conta.
		return HttpResponseRedirect("/administrador/login/")
		
	if request and request.is_ajax():
		bancoDeDados = connection.cursor()
		bancoDeDados.execute("INSERT INTO TELA_CIDADA.blog_postagem (titulo, texto, funcionario ,marcador,published_date,created_date) VALUES ('%s', '%s',%d,'%s',NOW())" % (str(request.POST['titulo']),str(request.POST['texto']),request.user.id, datetime.datetime.strptime(request.POST['dataCriacao'],'%d/%m/%Y').date()))
				
		response = HttpResponse()
		response.write(json.dumps('ok'))
		return response
	return render(request, 'xhtml/criarPost.xhtml', {})

def editarPost(request, offset):
	print offset
	if not request.user.is_authenticated(): # verifica se o usuario esta logado, se estiver ele sera deslogado da conta.
		return HttpResponseRedirect('/administrador/login/')
	
	bancoDeDados = connection.cursor()
	bancoDeDados.execute("SELECT * FROM TELA_CIDADA.blog_postagem WHERE id = '%d'" % (int(float(offset.split('_')[0]))))
	dicionarioPostagem = DictFetchAll(bancoDeDados)
	postagem = []
	for post in dicionarioPostagem:
		postagem.append( {"id": post['id'], "data": str(post['data']), "titulo": post['titulo'], "texto": post['texto']})
	return render(request, 'xhtml/editarPost.xhtml', {"postagem": json.dumps(postagem)})
	
def vizualizarPosts(request):
	if not request.user.is_authenticated(): # verifica se o usuario esta logado, se estiver ele sera deslogado da conta.
		return HttpResponseRedirect('/administrador/login/')
		
	bancoDeDados = connection.cursor()
	bancoDeDados.execute("SELECT Pessoa.nome, Pessoa.sobrenome, Postagem.data, Postagem.titulo, Postagem.id FROM TELA_CIDADA.blog_postagem Postagem INNER JOIN TELA_CIDADA.administrador_pessoa Pessoa ON Pessoa.id = Postagem.id")
	
	dicionarioPostagens = DictFetchAll(bancoDeDados)
	postagens = []
	
	for posts in dicionarioPostagens:
		dados = {"id": posts['id'], "autor": posts['nome'] + ' ' + posts['sobrenome'], "data": str(posts['data']), "titulo": posts['titulo']}
		postagens.append(dados)
	
	return render(request, 'xhtml/vizualizarPosts.xhtml', {"postagens":json.dumps(postagens)})
